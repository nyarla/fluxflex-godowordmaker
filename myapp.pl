#!/usr/bin/env perl

package GodowordMaker;

use strict;
use warnings;
use utf8;

use Acme::GodoWord;
use Lingua::JA::Moji qw/ hira2kata /;
use URI::Escape qw/ uri_escape_utf8 /;

use Mojolicious::Lite;

get '/' => 'index';

get '/generate' => sub {
    my $self = shift;
    return $self->redirect_to('index');
};

my %converters = map { $_ => "convert_${_}" } qw/ godoword yukio /;

post '/generate' => sub {
    my $self = shift;
    my $conv = $self->param('converter');
    my $msg  = $self->param('message');
       $msg  =~ s{\r?\n}{}gs;

    if ( exists $converters{$conv} && defined( my $method = $converters{$conv} ) ) {
        my $message = GodowordMaker->$method($msg);
        my $escaped = uri_escape_utf8($message . 'http://www.godowordmaker.com/');
        return $self->render('publish', message => $message, escaped => $escaped );
    }
    else {
        return $self->render('error');
    }
};

sub convert_godoword {
    my ( $class, $msg ) = @_;
    return Acme::GodoWord->babelize($msg);
}

my %chars    = qw/
    カ ガ
    キ ギ
    ク グ
    ケ ゲ
    コ ゴ
    サ ザ
    シ ジ
    ス ズ
    セ ゼ
    ソ ゾ
    タ ダ
    チ ヂ
    ツ ヅ
    テ デ
    ト ド
    ハ バ
    ヒ ビ
    フ ブ
    ヘ ベ
    ホ ボ
/;

sub convert_yukio {
    my ( $class, $msg ) = @_;
    
    $msg = hira2kata( $msg );

    for my $target ( keys %chars ) {
        $msg =~ s/${target}/$chars{$target}/gs;
    }

    return $msg;
}

app->start;

__DATA__

@@ layouts/main.html.ep
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">

    <style type="text/css">
body {
    text-align : center ;
}

a:link    { color : #0000FF ; }
a:visited { color : #800080 ; }
a:hover   { color : #0000FF ; }
a:active  { color : #FF0000 ; }

section, nav, article, aside,
hgourp, header, footer, address {
    display : block ;
    margin : 0 ;
    padding : 0 ;
}

header.banner h1 {
    font-size : 1.2em ;
}

header.banner h1 a {
    text-decoration : none ;
    
    font-size : 1em ;
    font-weight : bold ;

}

header.banner h1 a:link,
header.banner h1 a:visited { color : #000000 ; }
header.banner h1 a:hover   { color : #0000FF ; }
header.banner h1 a:active  { color : #FF0000   }

article.main {
    margin  : 3em auto ;
}

article.main form textarea {
    padding : 0.5em ;
    height : 3em ;
    width : 30em ;
}

footer.contentinfo p {
    font-size : 0.8em ;
}
    </style>

    <title><%= title %></title>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-323288-6']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>

  <header class="banner">
    <h1><a href="/">ごどーわーど☆めーかー</a></h1>
  </header>

  <article class="main">
    <%== content %>
  </article>

  <footer class="contentinfo">
    <p>作った人: <a href="http://nyarla.net/">岡村　直樹 (にゃるら、)</a></p>
    <p>連絡先: &lt;nyarla[ at ]thotep.net&gt;</p>
    <p>Hosting by: <a href="https://www.fluxflex.com/">fluxflex.com</a></p>
  </footer>

</body>
</html>

@@ error.html.ep
% layout 'main', title => 'Error !';

<h1>Error!</h1>

<p>対応するコンバーターが見つかりませんでした。</p>

@@ index.html.ep
% layout 'main', title => 'ごどーわーど☆めーかー';

<h1>何をつぶやきますか？</h1>

<form action="/generate" method="POST">
  <p>
    <label for="converter">変換先</label>
    <select name="converter" id="converter">
      <option value="godoword">統一言語</option>
      <option value="yukio">ユキオちゃん</option>
    </select>
  </p>
  <textarea name="message" cols="40" rows="2"></textarea>
  <p><input type="submit" value="生成！"></p>
</form>

@@ publish.html.ep
% layout 'main', title => '変換結果！';

<h1>変換結果！</h1>

<p class="published"><%= $message %></p>

<p class="tweet">
  <a href="http://twitter.com/home?status=<%= $escaped %>">Twitterでつぶやく</a>
</p>

<p><a href="/">戻る</a></p>
